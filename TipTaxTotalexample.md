

#### Design a program that calculates the total amount of a meal purchased at a restaurant. The program should ask the user to enter the charge for the food, and then calculate the amount of a 15 percent tip and 7 percent sales tax. Display each of these amounts and the total

**"BrainStrom" Algorithm ( outline)**
```
Ask user for food price
assigned price to food variable

Multiply food variable to tip

Multiply food variable to tax

Get the sum of Food + tip + tax

Show the output
Tip : $ tip
Tax : $ tax
Total : $ total
```
```
// Declare variables for food charges, tip, tax, and total.
Declare Real food, tip, tax, total

// Constants for the tax rate and tip rate.
Constant Real TAX_RATE = 0.07
Constant Real TIP_RATE = 0.15

// Get the food charges.
Display "Enter the charge for food."
Input food

// Calculate the tip.
Set tip = food * TIP_RATE

// Calculate the tax.
Set tax = food * TAX_RATE

// Calculate the total.
Set total = food + tip + tax

// Display the tip, tax, and total.
Display "Tip: $", tip
Display "Tax: $", tax
Display "Total: $", total
```