If the following pseudocode were an actual program, why would it not display the output that the programmer expects?
```
Declare String favoriteFood

Display "What is the name of your favorite food?"
Input favoriteFood

Display "Your favorite food is "
Display "favoriteFood"
```
If the programmer translates the following pseudocode to an actual programming language, a syntax error is likely to occur. Can you find the error?
```
Declare String 1stPrize

Display "Enter the award for first prize."
Input 1stPrize

Display "The first prize winner will receive ", 1stPrize
```

The following code will not display the results expected by the programmer. Can you find the error?
```
Declare Real lowest, highest, average

Display "Enter the lowest score."
Input lowest

Display "Enter the highest score."
Input highest

Set average = low + high / 2
Display "The average is ", average, "."
```

Find the error in the following pseudocode.
```
Display "Enter the length of the room."
Input length
Declare Integer length
```
Find the error in the following pseudocode.
```
Declare Integer value1, value2, value3, sum
Set sum = value1 + value2 + value3

Display "Enter the first value."
Input value1

Display "Enter the second value."
Input value2

Display "Enter the third value."
Input value3

Display "The sum of numbers is ", sum
```
Find the error in the following pseudocode.
```
Declare Real pi
Set 3.14159265 = pi
Display "The value of pi is ", pi
```
Find the error in the following pseudocode.
```
Constant Real GRAVITY = 9.81
Display "Rates of acceleration of an object in free fall:"
Display "Earth: ", GRAVITY, " meters per second every second."
Set GRAVITY = 1.63
Display "Moon: ", GRAVITY, " meters per second every second."
```

Declare and Initialize
```
Declare Integer num
Declare integer num1 = 23  // assigning a value to the variable is "initialization" 

```