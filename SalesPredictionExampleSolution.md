

// Variables to hold the sales total and the profit
Declare Real salesTotal, profit
Constant Real TAX = 0.30

// Get the amount of projected sales.
Display "Enter the projected sales."
Input salesTotal

// Calculate the projected profit.
Set profit = salesTotal * TAX

// Display the projected profit.
Display "The projected profit is ", profit


set annual = profit * TAX