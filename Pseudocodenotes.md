# Pseudocode

Get a bowl
stir
add two eggs
add a gallon of gasoline
bake at 350 degrees for 45 minutes
add three cups of flour


Program Development cycle

Syntax error - language specific error (eg. punctuation/space)
logic error - a mistake that does not prevent the program from running, but causes it to produce incorrect results( common mistakes are mathematical in nature).

Steps:
Design a program (Pseudocode/flowchart)
write the code
correct syntax errors
test the executable code
debug the code


# Calculate hourly pay 

```
Display "Enter the number of hours the employee worked."
Input hours
Display "Enter the employee's hourly pay rate."
Input payRate
Set grossPay = hours * payRate
Display "The employee's gross pay is $", grossPay

```

## Multiply a number
```
start
    input myNumber
    myAnswer = myNumber * 2
    output myAnswer
stop











