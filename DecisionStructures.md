## The Reason for Structure (organized design)

Clarity - The more programs grow in code the more confusing they can be without structure

Professionalism - All programmers expect your programs to be structured. 

Efficiency - improve the runtime of the application ( with big programs) 