 **Each file starts with some comments that describe the problem. Comments are lines that begin with two slashes (//). Following the comments, each file contains pseudocode that has one or more bugs you must find and correct.**

 Program #1
 ```
 // This pseudocode is intended to determine whether students have
// passed or failed a course; student needs to average 60 or
// more on two tests.
start
   Declarations
      num firstTest
      num secondTest
      num average
      num PASSING = 60
   while firstTest not equal to 0
      output "Enter first score or 0 to quit "
      input firstTest
      output "Enter second score"
      input secondTest
      average = (firstTest + secondTest) / 2
      ouput "Average is ", average
      if average >= PASSING then
         output "Pass"
      else
         output "Fail"
      endif
   endwhile
stop
```
Program #2
```
// This pseudocode is intended to display employee net pay values.
// All employees have a standard $45 deduction from their checks.
// If an employee does not earn enough to cover the deduction,
// an error message is displayed.
start
   Declarations
      string name
      num hours
      num rate
      string DEDUCTION = 45
      string EOFNAME = "ZZZ"
      num gross
      num net
   output "Enter first name or ", EOFNAME, " to quit"
   input name
   if name not equal to EOFNAME
      output "Enter hours worked for ", name
      input hours
      output "Enter hourly rate for ", name
      input rate
      gross = hours * rate
      net = gross - DEDUCTION
      while net > 0 then
         output "Net pay for ", name, " is ", net
      else
          output "Deductions not covered. Net is 0."
      endwhile
      output "Enter next name or ", EOFNAME, " to quit"
      input name
   endif
   output "End of job"
stop
```
Program #3
```
// This pseudocode is intended to display
// employee net pay values. All employees have a standard
// $45 deduction from their checks.
// If an employee does not earn enough to cover the deduction,
// an error message is displayed.
// This example is modularized.
start
   Declarations
      string name
      string EOFNAME = ZZZZ
   while name not equal to EOFNAME
      housekeeping()
   endwhile
   while name not equal to EOFNAME
      mainLoop()
   endwhile
   while name not equal to EOFNAME
      finish()
   endwhile
stop

housekeeping()
   output "Enter first name or ", EOFNAME, " to quit "
return

mainLoop()
   Declarations
      num hours
      num rate
      num DEDUCTION = 45
      num net
   output "Enter hours worked for ", name
   input hours
   output "Enter hourly rate for ", name
   input rate
   gross = hours * rate
   net = gross - DEDUCTION
   if net > 0 then
      output "Net pay for ", name, " is ", net
   else
      output "Deductions not covered. Net is 0."
   endif
   output "Enter next name or ", EOFNAME, " to quit "
   input name
return

finish()
   output "End of job"
return

```
