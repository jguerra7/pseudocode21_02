1. Identify the problem. How would you fix the problem if you were to translate this pseudocode into one of the aforementioned languages?
```
Module checkEquality(Integer num1, Integer num2)
   If num1 = num2 Then
      Display "The values are equal."
   Else
      Display "The values are NOT equal."
   End If
End Module
```
2. The intended purpose of the following module is to set the temp parameter to the value 32.0 if it is not already equal to 32.0. This will not work as the programmer expects, however. Find the problem.
```
Module resetTemperature(Real Ref temp)
   If NOT temp == 32.0 Then
      Set temp = 32.0
   End If
End Module
```
3. The intended purpose of the following module is to determine whether the value parameter is within a specified range. The module will not work, however. Find the problem.
```
Module checkRange(Integer value, Integer lower, Integer upper)
   If value < lower AND value > upper Then
      Display "The value is outside the range."
   Else
      Display "The value is within the range."
   End If
End Module
```