Kathryn teaches a science class and her students are required to take three tests. She wants to write a program that her students can use to calculate their average test score. She also wants the program to congratulate the student enthusiastically if the average is greater than 95
```
Get the first test score.

Get the second test score.

Get the third test score.

Calculate the average.

Display the average.

If the average is greater than 95, congratulate the user.
```

### GetAvg program
```
 1  // Declare variables
2  Declare Real test1, test2, test3, average
3
 4  // Get test 1
5  Display "Enter the score for test #1."
6  Input test1
7
 8  // Get test 2
9  Display "Enter the score for test #2."
10  Input test2
11
12  // Get test 3
13  Display "Enter the score for test #3."
14  Input test3
15
16  // Calculate the average score.
17  Set average = (test1 + test2 + test3) / 3
18
19  // Display the average.
20  Display "The average is ", average
21
22  // If the average is greater than 95
23  // congratulate the user.
24  If average > 95 Then
25     Display "Congratulations! Great average!"
26  End If


```
### GetAvg output
```
Enter the score for test #1.
82 [Enter] 
Enter the score for test #2.
76 [Enter] 
Enter the score for test #3.
91 [Enter] 
The average is 83
```
```
Enter the score for test #1.
93 [Enter] 
Enter the score for test #2.
99 [Enter] 
Enter the score for test #3.
96 [Enter] 
The average is 96
Congratulations! Great average!

```

**Consider a program that determines whether a bank customer qualifies for a loan. To qualify, two conditions must exist: (1) the customer must earn at least $30,000 per year, and (2) the customer must have been employed at his or her current job for at least two years**

Loan qualification program
```
 1  // Declare variables
 2  Declare Real salary, yearsOnJob
 3
 4  // Get the annual salary.
 5  Display "Enter your annual salary."
 6  Input salary
 7
 8  // Get the number of years on the current job.
 9  Display "Enter the number of years on your"
10  Display "current job."
11  Input yearsOnJob
12
13  // Determine whether the user qualifies.
14  If salary >= 30000 Then
15     If yearsOnJob >= 2 Then
16        Display "You qualify for the loan."
17     Else
18        Display "You must have been on your current"
19        Display "job for at least two years to qualify."
20     End If
21  Else
22     Display "You must earn at least $30,000"
23     Display "per year to qualify."
24  End If

```
```
Enter your annual salary.
35000 [Enter] 
Enter the number of years on your
current job.
1 [Enter] 
You must have been on your current
job for at least two years to qualify.

```

```
Enter your annual salary.
35000 [Enter] 
Enter the number of years on your
current job.
5 [Enter] 
You qualify for the loan.
```

Loan program that is logically correct but with certain refactoring issues. 

```
If salary >= 30000 Then
If yearsonJob >= 2 Then
Display "You qualify for the loan."
Else
Display "You must have been on your current"
Display "job for at least two years to qualify."
End If
Else
Display "You must earn at least $30,000"
Display "per year to qualify.
End If

```
output:
```
Enter your annual salary.
25000 [Enter] 
Enter the number of years on your
current job.
5 [Enter] 
You must earn at least $30,000
per year to qualify.
```

### GRADE PROGRAM

Dr. Suarez teaches a literature class and uses the following 10 point grading scale for all of his exams:
```
>=90  -> A
80-89 -> B
70-79 -> C
60-69 -> D
below 60 -> F

```
```
Ask the user to enter a test score.

Determine the grade in the following manner:

 
If the score is less than 60, then the grade is “F.”

   
Otherwise, if the score is less than 70, then the grade is “D.”

    Otherwise, if the score is less than 80, then the grade is “C.”

     Otherwise, if the score is less than 90, then the grade is “B.”

      Otherwise, the grade is “A.”
```

## Grade program pseudocode
```
 1  // Variable to hold the test score
 2  Declare Real score
 3
 4  // Get the test score.
 5  Display "Enter your test score."
 6  Input score
 7
 8  // Determine the grade.
 9  If score < 60 Then
10     Display "Your grade is F."
11  Else
12     If score < 70 Then
13        Display "Your grade is D."
14     Else
15        If score < 80 Then
16           Display "Your grade is C."
17        Else
18           If score < 90 Then
19              Display "Your grade is B."
20           Else
21              Display "Your grade is A."
22           End If
23        End If
24     End If
25  End If
```
## Comparing Strings (text/words)

```
Declare String name1 = "Mary"
Declare String name2 = "Mark"
If name1 == name2 Then
   Display "The names are the same."
Else
   Display "The names are NOT the same."
End If

```
Password login program

```
 1  // A variable to hold a password.
 2  Declare String password
 3
 4  // Prompt the user to enter the password.
 5  Display "Enter the password."
 6  Input password
 7
 8  // Determine whether the correct password
 9  // was entered.
10  If password == 1234 Then
11     Display "Password accepted."
12  Else
13     Display "Sorry, that is not the correct password." 
14  End If

```
## Refactor Loan program
```
1  // Declare variables
 2  Declare Real salary, yearsOnJob
 3
 4  // Get the annual salary.
 5  Display "Enter your annual salary."
 6  Input salary
 7
 8  // Get the number of years on the current job.
 9  Display "Enter the number of years on your"
10  Display "current job."
11  Input yearsOnJob
12
13  // Determine whether the user qualifies.
14  If salary >= 30000 AND yearsOnJob >= 2 Then
15     Display "You qualify for the loan."
16  Else
17     Display "You do not qualify for this loan." 
18  End If

```