### Scientists have determined that the world’s ocean levels are currently rising at about 1.5 millimeters per year.


Write a program to display the following:
```
The number of millimeters that the oceans will rise in five years

The number of millimeters that the oceans will rise in seven years

The number of millimeters that the oceans will rise in ten years
```

Algorithm: ( still needs work in design)
```
1.Calculate the amount that the oceans will rise in five years.

2.Display the result of the calculation in Step 1.

3.Calculate the amount that the oceans will rise in seven years.

4.Display the result of the calculation in Step 3.

5.Calculate the amount that the oceans will rise in ten years.

6.Display the result of the calculation in Step 5.
```

#### Amount of yearly rise X Number years

```
 1  // Declare the variables
 2  Declare Real fiveYears
 3  Declare Real sevenYears
 4  Declare Real tenYears
 5
 6  // Create a constant for the yearly rise
 7  Constant Real YEARLY_RISE = 1.5
 8
 9  // Display the amount of rise in five years
10  Set fiveYears = YEARLY_RISE * 5
11  Display "The ocean levels will rise ", fiveYears,
12          " millimeters in five years."
13
14  // Display the amount of rise in seven years
15  Set sevenYears = YEARLY_RISE * 7
16  Display "The ocean levels will rise ", sevenYears,
17          " millimeters in seven years."
18
19  // Display the amount of rise in ten years
20  Set tenYears = YEARLY_RISE * 10
21  Display "The ocean levels will rise ", tenYears,
22          " millimeters in ten years."


```

**Program Output**
```
The ocean levels will rise 7.5 millimeters in five years.
The ocean levels will rise 10.5 millimeters in seven years.
The ocean levels will rise 15 millimeters in ten years.
```