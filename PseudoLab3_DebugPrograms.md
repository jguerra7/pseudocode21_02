## Program 1

```
// This pseudocode segment is intended to compute and display
// the average grade of three tests
start
   Declarations
      num test1
      num test2
      num test2
      num average
   output "Enter score for test 1 
   input test1
   output "Enter score for test 2 "
   input test2
   output "Enter score for test 3 "
   input test3
   average = test1 + test2 + test3 / 3
   output "Average is ", answer
end

```
## Program 2

```
// This pseudocode segment is intended to compute and display
// the average grade of three tests for any number of students.
// The program executes until the user enters a negative value
// for the first test score.
start
   Declarations
      num test1
      num test2
      num test3
      num average
   housekeeping()
   while test1 >= 0
      mainLoop()
   endwhile
   endOfJob()
stop

housekeeping()
   output "Enter score for test 1 or a negative number to quit"
return

mainLoop()
   output "Enter score for test 2"
   input test2
   average = (test1 + test2 + test3) / 3
   output "Average is ", average
   output "Enter score for test 1 or a negative number to quit"
   input tesst1
return

endOfJob()
   output "End of program"
return


```

## Program 3

```
// This pseudocode segment is intended to compute and display
// the cost of home ownership for any number of users
// The program ends when a user enters 0 for the mortgage payment
start
   Declarations
      num mortgagePayment
      num utilities
      num taxes
      num upkeep
      num total
   startup()
   while mortgagePayment not equal to 0
      MainLoop()
   endwhile
   finishUp()
stop

startUp()
   output "Enter your mortgage payment or 0 to quit"
   input mtgPayment
return

mainLoop()
   output "Enter utilities"
   input utilities
   output "Enter taxes"
   input taxes
   output "Enter amount for upkeep"
   input upkeep
   total = mortgagePayment + utilities + taxes + upkeep
   output "Total is ", total
return

finishUp()
   output "End of program"
return

```