
In baseball, batting average is commonly used to measure a player’s batting ability. You use the following formula to calculate a player’s batting average:
```
Batting avg = Hits / Times at bat
```
Common scenario:
```
Input is received.

Some process (such as a calculation) is performed on the input.

Output is produced.
```

BrainStorm Algorithm
```
1. Input is received
    The number of hits
    The number of times at bat
2. Some process (such as a calculation) is performed on the input.
    Calculate "Batting average" from the formula
3. Output is produced
    The output of the batting average program will be the result of the calculation, which is stored in a variable named "battingAverage"

```

Working on the parts:
```
//setting up the variables
Declare Integer hits
Declare Integer atBat
Declare Real battingAverage
```
Reading the information given:
```
//Prompting the user for information
Display "Enter the player's number of hits."
Input hits

Display "Enter the player's number of times at bat."
Input atBat
```
Calculation:
```
Set battingAverage = hits / atBat
```
output:
```
Display "The player's batting average is ", battingAverage
```

**Putting it all together**

```
 1  // Declare the necessary variables.
 2  Declare Integer hits
 3  Declare Integer atBat
 4  Declare Real battingAverage
 5
 6  // Get the number of hits.
 7  Display "Enter the player's number of hits."
 8  Input hits
 9
10  // Get the number of times at bat.
11  Display "Enter the player's number of times at bat."
12  Input atBat
13
14  // Calculate the batting average.
15  Set battingAverage = hits / atBat
16
17  // Display the batting average.
18  Display "The player's batting average is ", battingAverage

```
**Program Output**
```
Enter the player's number of hits.
150 [Enter] 
Enter the player's number of times at bat.
500 [Enter] 
The player's batting average is 0.3
```

As a software developer, whenever you have trouble getting started with a program design, determine the program’s requirements as follows:

1.**Input:** Carefully study the problem and identify the pieces of data that the program needs to read as input. Once you know what data is needed as input, decide the names of the variables for those pieces of data, and their data types.

2.**Process:** What must the program do with the input that it will read? Determine the calculations and/or other processes that must be performed. At this time, decide the names and data types of any variables needed to hold the results of calculations.

3.**Output:** What output must the program produce? In most cases, it will be the results of the program’s calculations and/or other processes.