
### Team Software Project
Health and Fitness pseudocode project

Rubric:
```
- Start a Gitlab for the project
- Document your division of labor and progress
- Create branches for the features
- Push and Pull code as a team to check/test/change code
- Short presentation of your project

```


**Apply the agile scrum methodology while using gitlab to create the pseudocode for this project**


A Health & Fitness company wants a software application that provides the body mass index(BMI) of an individual, Calculates calories from Fat and Carbohydrates, and calculate the number of calories burned from running. 

They also have a "Cookie calorie calculator" program that works and they like it, however would want a generic form of it so they can use to accept any food to calculate the calories. 


**Calories Burned**

Running on a particular treadmill you burn 3.9 calories per minute. Design a feature that uses a loop to display the number of calories burned after 10, 15, 20, 25, and 30 minutes.

**Body Mass Index**

a feature that calculates and displays a person’s body mass index (BMI). The BMI is often used to determine whether a person with a sedentary lifestyle is overweight or underweight for his or her height. A person’s BMI is calculated with the following formula:

**Formula:**
```
BMI = Weight X 703/height^2

```
**Calories from Fat and Carbohydrates**
 calculate the number of calories that result from the fat, and Carbohydrates from the following formulas:
 ```
 Calories from Fat = Fat Grams X 9

 Calories from Carbohydrates = Carbohydrate grams X 4
 ```
 Design a feature that will make these calculations


 **Cookie Calories calculator**

// Constant for the number of cookies per bag
Constant Integer COOKIES_PER_BAG = 40
        
// Constant for the number of servings per bag
Constant Integer SERVINGS_PER_BAG = 10

// Constant for the number of calories per serving
Constant Integer CALORIES_PER_SERVING = 300
      
// Constant for the number of cookies per serving
Constant Integer COOKIES_PER_SERVING = 
                 COOKIES_PER_BAG / SERVINGS_PER_BAG
      
// Constant for the number of calories per cookie
Constant Integer CALORIES_PER_COOKIE = 
                 CALORIES_PER_SERVING / COOKIES_PER_SERVING

// Variables
Declare Integer cookiesEaten		// Cookies eaten
Declare Integer totalCalories		// Calories consumed
     
// Get the number of cookies eaten.
Display "Enter the number of cookies eaten: "
Input cookiesEaten
      
// Calculate the number of total calories consumed.
Set totalCalories = cookiesEaten * CALORIES_PER_COOKIE
      
// Display the number of total calories consumed.
Display "Total calories consumed: ", totalCalories

