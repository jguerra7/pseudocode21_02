### Programming Performance Labs

**Roman Numerals**

Design a program that prompts the user to enter a number within the range of 1 through 10. The program should display the Roman numeral version of that number. If the number is outside the range of 1 through 10, the program should display an error message.

**Areas of Rectangles**

The area of a rectangle is the rectangle’s length times its width. Design a program that asks for the length and width of two rectangles. The program should tell the user which rectangle has the greater area, or whether the areas are the same.

**Magic Dates**

The date June 10, 1960, is special because when it is written in the following format, the month times the day equals the year:
```
6/10/60

```
Design a program that asks the user to enter a month (in numeric form), a day, and a two-digit year. The program should then determine whether the month times the day equals the year. If so, it should display a message saying the date is magic. Otherwise, it should display a message saying the date is not magic.

**Color Mixer**

The colors red, blue, and yellow are known as the primary colors because they cannot be made by mixing other colors. When you mix two primary colors, you get a secondary color, as shown here:
```
When you mix red and blue, you get purple.

When you mix red and yellow, you get orange.

When you mix blue and yellow, you get green.

```
Design a program that prompts the user to enter the names of two primary colors to mix. If the user enters anything other than “red,” “blue,” or “yellow,” the program should display an error message. Otherwise, the program should display the name of the secondary color that results.

**Guessing game**
write pseudocode that describes the process of guessing a number between 1 and 100. After each guess, the player is told that the guess is too high or too low. The process continues until the player guesses the correct number. 
Follow the instructions yourself to play the game or
(Pick a number and have a fellow student try to guess it by following your instruction)

**Looking up a Word**
Looking up a word in a dictionary can be a complicated process. For example, assume that you want to look up logic. You might open the dictionary to a random page and see juice. You know this word comes alphabetically before logic, so you flip forward and see lamb. That is still not far enough, so you flip forward and see monkey. You have gone too far, so you flip back, and so on. Draw a structured flowchart or write pseudocode that describes the process of looking up a word in a dictionary. Pick a word at random and have a fellow student attempt to carry out your instructions.
